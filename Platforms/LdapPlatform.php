<?php

namespace Odm\Bundle\OdmBundle\Platforms;

use Doctrine\DBAL\Platforms\AbstractPlatform;

class LdapPlatform extends AbstractPlatform
{
    public function getBooleanTypeDeclarationSQL(array $columnDef)
    {

    }

    public function getIntegerTypeDeclarationSQL(array $columnDef)
    {

    }

    public function getBigIntTypeDeclarationSQL(array $columnDef)
    {

    }

    public function getSmallIntTypeDeclarationSQL(array $columnDef)
    {

    }

    public function _getCommonIntegerTypeDeclarationSQL(array $columnDef)
    {

    }

    public function initializeDoctrineTypeMappings()
    {

    }

    public function getClobTypeDeclarationSQL(array $field)
    {

    }

    public function getBlobTypeDeclarationSQL(array $field)
    {

    }

    public function getName()
    {
        return 'ldap';
    }

    /**
     * {@inheritDoc}
     *
     * MySql prefers "autoincrement" identity columns since sequences can only
     * be emulated with a table.
     */
    public function prefersIdentityColumns()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     *
     * MySql supports this through AUTO_INCREMENT columns.
     */
    public function supportsIdentityColumns()
    {
        return true;
    }
}