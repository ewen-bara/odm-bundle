<?php

namespace Odm\Bundle\OdmBundle;

use Odm\Bundle\OdmBundle\DependencyInjection\OdmExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle.
 */
class OdmBundle extends Bundle
{
    /** @var callable|null */
    private $autoloader;

    /**
     * {@inheritDoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            return new OdmExtension();
        }

        return $this->extension;
    }
}