<?php

namespace Odm\Bundle\OdmBundle\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Type that maps an SQL VARCHAR to a PHP string.
 *
 * @since 2.0
 */
class StringType extends \Doctrine\DBAL\Types\StringType
{
    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = Type::getType('ldap_array')->convertToPHPValue($value, $platform);
        return parent::convertToPHPValue($value[0], $platform);
    }
}
