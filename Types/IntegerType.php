<?php

namespace Odm\Bundle\OdmBundle\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\ParameterType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\PhpIntegerMappingType;

/**
 * Type that maps an SQL INT to a PHP integer.
 *
 * @author Roman Borschel <roman@code-factory.org>
 * @since 2.0
 */
class IntegerType extends \Doctrine\DBAL\Types\IntegerType
{
    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = Type::getType('ldap_array')->convertToPHPValue($value, $platform);
        return parent::convertToPHPValue($value[0], $platform);
    }
}
