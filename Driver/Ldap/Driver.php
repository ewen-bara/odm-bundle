<?php

namespace Odm\Bundle\OdmBundle\Driver\Ldap;

use Doctrine\DBAL\Driver as DBALDriver;
use Doctrine\DBAL\Connection;
use Odm\Bundle\OdmBundle\Platforms\LdapPlatform;
use Odm\Bundle\OdmBundle\Driver\LdapConnection;

class Driver implements DBALDriver
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
        return new LdapConnection(
            $this->constructPdoDsn($params),
            $params['dbname'],
            $username,
            $password,
            $driverOptions
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ldap';
    }

    public function getDatabasePlatform()
    {
        return new LdapPlatform();
    }

    public function getSchemaManager(Connection $conn)
    {

    }

    public function getDatabase(Connection $conn)
    {
        return $conn->getParams()['dbname'];
    }

    /**
     * Constructs the LDAP URL.
     *
     * @param array $params
     *
     * @return string The DSN.
     */
    protected function constructPdoDsn(array $params)
    {
        if($params['sslmode'] == 'ssl') {
            $dsn = 'ldaps://';
        } else {
            $dsn = 'ldap://';
        }

        if (isset($params['host']) && $params['host'] != '') {
            $dsn .= $params['host'];
        }
        if (isset($params['port'])) {
            $dsn .= ':' . $params['port'];
        }

        return $dsn;
    }
}