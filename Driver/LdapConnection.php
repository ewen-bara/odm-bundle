<?php

namespace Odm\Bundle\OdmBundle\Driver;

use Symfony\Component\Ldap\Ldap;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Driver\ServerInfoAwareConnection;

class LdapConnection implements Connection, ServerInfoAwareConnection
{
    private $_conn;
    private $_dbname;

    public function __construct($dsn, $dbname = null, $user = null, $password = null, array $options = null)
    {
        try {
            $this->_conn = Ldap::create('ext_ldap', array('connection_string' => $dsn));
            $this->_conn->bind($user, $password);
            $this->_dbname = $dbname;
        } catch (Symfony\Component\Ldap\Exception\ConnectionException $exception) {
            throw new Symfony\Component\Ldap\Exception\ConnectionException($exception);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function exec($statement)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getServerVersion()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function prepare($prepareString, $driverOptions = [])
    {
        return new LdapStatementConverter($this->_conn, $this->_dbname, $prepareString);
    }

    /**
     * {@inheritdoc}
     */
    public function query()
    {
        $args = func_get_args();
        $argsCount = count($args);

        //*
        $stmt = new LdapStatementConverter($this->_conn, $this->_dbname, $args[0]);
        $stmt->execute();
        return $stmt;
        /*/
        if ($argsCount == 3) {
            return $this->_conn->query($args[0], $args[1], $args[2]);
        }

        if ($argsCount == 2) {
            return $this->_conn->query($args[0], $args[1]);
        }
        //*/
    }

    /**
     * {@inheritdoc}
     */
    public function quote($input, $type = ParameterType::STRING)
    {
        return $input;
    }

    /**
     * {@inheritdoc}
     */
    public function lastInsertId($name = null)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function requiresQueryForServerVersion()
    {
        return true;
    }

    public function beginTransaction()
    {

    }

    public function commit()
    {

    }

    public function rollback()
    {

    }

    public function errorCode()
    {

    }

    public function errorInfo()
    {

    }
}