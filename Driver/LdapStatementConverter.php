<?php

namespace Odm\Bundle\OdmBundle\Driver;

use Doctrine\DBAL\Driver\Statement;
use Symfony\Component\Ldap\Entry;

class LdapStatementConverter implements \IteratorAggregate, Statement
{
    private $conn;
    private $dbname;
    private $prepareString;
    private $params;
    private $fetchMode;
    private $results;

    private $attrMap;
    private $query;
    private $pos = 0;

    public function __construct($conn, $dbname, $prepareString)
    {
        $this->conn = $conn;
        $this->dbname = $dbname;
        $this->prepareString = $prepareString;
        $this->query = $prepareString;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function bindValue($param, $value, $type = ParameterType::STRING)
    {
        $this->query = preg_replace('/\?/', $value, $this->query, 1);
    }

    /**
     * {@inheritdoc}
     */
    public function bindParam($column, &$variable, $type = ParameterType::STRING, $length = null)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function errorCode()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function errorInfo()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function execute($params = null)
    {
        if (!empty($params)) {
            foreach ($params as $param => $value) {
                $this->bindValue($param, $value);
            }
        }

        if (preg_match('/^SELECT/', $this->query)) {
            $this->select();
        } else if (preg_match('/^INSERT INTO/', $this->query)) {
            $this->insert();
        } else if (preg_match('/^UPDATE/', $this->query)) {
            $this->update();
        } else if (preg_match('/^DELETE/', $this->query)) {
            $this->delete();
        } else {
            $this->filter();
            // var_dump($this->query); die();
            //TODO: Throw execption
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rowCount()
    {
        if($this->results != null) {
            return count($this->results);
        } else {
            return 0;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function closeCursor()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function columnCount()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function setFetchMode($fetchMode, $arg2 = null, $arg3 = null)
    {
        $this->fetchMode = $fetchMode;
    }

    /**
     * {@inheritdoc}
     */
    public function fetch($fetchMode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
        $result = array();
        if($this->pos < $this->rowCount()) {
            foreach($this->results[$this->pos]->getAttributes() as $key => $value) {
                $result[$this->attrMap[$key]] = serialize($value);
            }
            $result = array_merge(array('dn_1' => $this->results[$this->pos]->getDn()), $result);
            $this->pos++;
            return $result;
        }
        return false;
    }

    /**
     * {@inheritdoc}fetchMode
     */
    public function fetchAll($fetchMode = null, $fetchArgument = null, $ctorArgs = null)
    {
        $results = array();

        $r = $this->fetch();
        while($r != null) {
            $results[] = $r;
            $r = $this->fetch();
        }
        return $results;
    }

    /**
     * {@inheritdoc}
     */
    public function fetchColumn($columnIndex = 0)
    {

    }

    private function filter() {
        $query = $this->conn->query($this->dbname, $this->query);

        $this->results = $query->execute()->toArray();
    }

    private function select() {
        $base = $this->dbname;
        $select = array();
        $from = '';
        $where = '';

        foreach(explode(',', preg_filter('/^SELECT ([^\n]*) FROM [^\n]*/', '$1', $this->query)) as $attr) {
            $attr = explode(' ', trim($attr));
            $name = explode('.', $attr[0])[1];
            $alias = $attr[2];
            $this->attrMap[$name] = $alias;
        }

        $from = $this->from($this->query);


        $filter = "(&(objectClass=$from)";

        $or = explode(' OR ', preg_filter('/[^\n]* WHERE ([^\0]*)[^\n]*/', '$1', $this->query));
        if(count($or) > 1) {
            $filter .= '(|';
        }
        if($or[0] != '') {
            foreach($or as $o) {
                $and = explode(' AND ', $o);
                if(count($and) > 1) {
                    $filter .= '(&';
                }
                foreach($and as $a) {
                    if(explode('=', explode('.', str_replace(' = ', '=', $a))[1], 2)[0] == 'dn') {
                        $base = explode('=', str_replace(' = ', '=', $a), 2)[1];
                    }
                    else {
                        $filter .= '('.explode('.', str_replace(' = ', '=', $a))[1].')';
                    }
                }
                if(count($and) > 1) {
                    $filter .= ')';
                }
            }
            if(count($or) > 1) {
                $filter .= ')';
            }
        }
        $filter .= ')';

        $query = $this->conn->query($base, $filter, array('filter' => array_keys($this->attrMap)));

        $this->results = $query->execute()->toArray();
    }

    private function insert()
    {

    }

    private function update()
    {
        $batch = array();
        $dn = explode(
            '=',
            str_replace(
                ' ',
                '',
                preg_filter(
                    '/[^\n]* WHERE ([^\0]*)[^\n]*/',
                    '$1',
                    $this->query
                )
            ),
            2
        )[1];

        $values = explode(', ', explode('WHERE', explode('SET', $this->query)[1])[0]);

        foreach ($values as $set) {
            list($attr, $value) = explode(' = ', $set);
            $batch[] =  new UpdateOperation(LDAP_MODIFY_BATCH_REPLACE, trim($attr), array(trim($value)));
        }

        // var_dump($dn, $batch); die();

        // Adding multiple email adresses at once
        $entryManager = $this->conn->getEntryManager();
        $entryManager->applyOperations($dn, $batch);
    }

    private function delete()
    {
        $this->conn->getEntryManager()->remove(
            new Entry(
                explode(
                    '=',
                    str_replace(
                        ' ',
                        '',
                        preg_filter(
                            '/[^\n]* WHERE ([^\0]*)[^\n]*/',
                            '$1',
                            $this->query
                        )
                    ),
                    2
                )[1]
            )
        );
    }

    private function from($query)
    {
        return preg_filter('/[^\n]* FROM ([^\ ]*) [^\n]*/', '$1', $query);
    }

    private function where()
    {

    }
}