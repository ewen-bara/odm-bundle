<?php

namespace Odm\Bundle\OdmBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Ldap\Ldap;
use Symfony\Component\Ldap\Entry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceEntryRepository extends ServiceEntityRepository
{
    protected function entry2object(Entry $entry, $class)
    {
        $object = new $class();
        $class_methods = get_class_methods($object);

        $object->setDn($entry->getDn());

        foreach($class_methods as $class_method) {
            if(substr($class_method, 0, 3) === 'set' && $entry->hasAttribute(lcfirst(substr($class_method, 3)))) {
                $attr = $entry->getAttribute(lcfirst(substr($class_method, 3)));
                if($this->_class->getTypeOfColumn(lcfirst(substr($class_method, 3))) === 'array'){
                    $object->$class_method($attr);
                }
                else
                {
                    $object->$class_method($attr[0]);
                }
            }
        }
        return $object;
    }

    protected function object2entry()
    {

    }
}
